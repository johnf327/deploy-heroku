Learning:

#####In order for the git lab runner to run docker you need to allow docker to run as a non root user by creating a new group and add the git lab runner to that group:

- sudo groupadd docker
- sudo usermod -aG docker $USER

#####More info below:

- https://docs.gitlab.com/ee/ci/docker/using_docker_build.html
- https://docs.docker.com/install/linux/linux-postinstall/

####Note you need to restart the VM

##### TODO: set up the heroku image in my other project, may need to set up connect to heroku properly"

- https://hub.docker.com/r/wingrunr21/alpine-heroku-cli

##### TODO: think I need to configure the runner to user the gitlab images! Just need to run a heroku command!

- https://docs.gitlab.com/ee/ci/docker/using_docker_images.html

#####NOT: I think I  need to reregister the runner every time because I  get a new IP address when I start the VM again


